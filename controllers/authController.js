const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const {PrismaClient} = require("@prisma/client");
require("dotenv").config({path: ".env"});
const prisma = new PrismaClient();

exports.register = async (req, res) => {
    console.log("TEST ", req.body);
    try {
        let {email, name, image, password, expoPushToken} = req.body;
        // console.log("Masuk Data Sign Up : ",req.body)
        if (!email || !password) return res.status(400).json({msg: "Not all fields have been entered."});

        // console.log("TEST");
        const existingAdmin = await prisma.user.findUnique({where: {email: email}});
        console.log("Existing Admin : ", existingAdmin);
        if (existingAdmin) return res
            .status(400)
            .json({msg: "An account with this email already exists."});

        const salt = await bcrypt.genSalt();
        const hashedPassword = await bcrypt.hash(password, salt);

        const newAdmin = await prisma.user.create({
            data: {
                email, name, image, hashedPassword, expoPushToken, emailVerified: new Date(), // Set emailVerified to the current date and time
            },
        });

        console.log("Data save : ", newAdmin);
        res.status(200).send({
            success: true, admin: {
                id: newAdmin.id,
                email: newAdmin.email,
                name: newAdmin.name,
                image: newAdmin.image,
                expoPushToken: newAdmin.expoPushToken,
                password: newAdmin.hashedPassword
            },
        });
    } catch (err) {
        res.status(500).json({
            success: false, result: null, message: err.message,
        });
    }
};


exports.login = async (req, res) => {
    try {
        const {email, password} = req.body;
        console.log("APA")
        // if (email && password) {
        //     // Validate
        //     if (!email || !password) return res.status(400).json({msg: "Not all fields have been entered."});
        //
        //     const client = await prisma.user.findUnique({where: {email}});
        //
        //     if (!client) return res.status(400).json({
        //         success: false, result: null, message: "No account with this email has been registered.",
        //     });
        //
        //     if (client.confirmation === false) return res.status(400).json({
        //         success: false, result: null, message: "Waiting for confirmation data.",
        //     });
        //
        //     const isMatch = await bcrypt.compare(password, client.hashedPassword);
        //     if (!isMatch) return res.status(400).json({
        //         success: false, result: null, message: "Invalid credentials.",
        //     });
        //
        //     const token = jwt.sign({
        //         exp: Math.floor(Date.now() / 1000) + 60 * 60 * 12, // 12 hours
        //         id: client.id,
        //     }, process.env.JWT_SECRET);
        //
        //     const result = await prisma.user.update({
        //         where: {id: client.id}, data: {isLoggedIn: true},
        //     });
        //
        //     console.log("Result : ", result);
        //     let finalresult;
        //     if (result) {
        //         console.log("Test");
        //         finalresult = await prisma.user.findUnique({
        //             where: {id: client.id}, include: {
        //                 imageProfile: true, imageSrc: true,
        //             },
        //         });
        //
        //         console.log("Final : ", finalresult);
        //     }
        //
        //     res.json({
        //         success: true, result: {
        //             user: {
        //                 id: result.id,
        //                 name: result.name,
        //                 phone: result.phone,
        //                 email: result.email,
        //                 image: result.image,
        //                 isLoggedIn: result.isLoggedIn,
        //             },
        //         }, message: "Successfully logged in as client",
        //     });
        // }
    } catch (err) {
        res.status(500).json({success: false, result: null, message: err.message});
    }
};

exports.isValidToken = async (req, res, next) => {
    try {
        const token = req.header("x-auth-token");
        if (!token) return res.status(401).json({
            success: false, result: null, message: "No authentication token, authorization denied.", jwtExpired: true,
        });

        const verified = jwt.verify(token, process.env.JWT_SECRET);
        if (!verified) return res.status(401).json({
            success: false, result: null, message: "Token verification failed, authorization denied.", jwtExpired: true,
        });

        const user = await prisma.user.findUnique({_id: verified.id});
        if (!user) return res.status(401).json({
            success: false, result: null, message: "User doens't Exist, authorization denied.", jwtExpired: true,
        });

        if (user.isLoggedIn === false) return res.status(401).json({
            success: false,
            result: null,
            message: "Admin is already logout try to login, authorization denied.",
            jwtExpired: true,
        }); else {
            req.user = user;
            // console.log(req.admin);
            next();
        }
    } catch (err) {
        res.status(500).json({
            success: false, result: null, message: err.message, jwtExpired: true,
        });
    }
};

exports.logout = async (req, res) => {
    console.log("Test : ", req.body);
    const {userId, travelType} = req.body;
    if (travelType === 'tourguide') {
        console.log("Test");
        try {
            const updatedUser = await prisma.client.update({
                where: {id: userId}, data: {isLoggedIn: false},
            });

            res.status(200).json({isLoggedIn: updatedUser.isLoggedIn});
        } catch (error) {
            console.error('Error:', error);
            res.status(500).json({error: 'An error occurred while logging out'});
        }
    } else {
        try {
            const updatedUser = await prisma.user.update({
                where: {id: userId}, data: {isLoggedIn: false},
            });

            res.status(200).json({isLoggedIn: updatedUser.isLoggedIn});
        } catch (error) {
            console.error('Error:', error);
            res.status(500).json({error: 'An error occurred while logging out'});
        }
    }
};