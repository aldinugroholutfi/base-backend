const userController = require('../controllers/userController');

// Array of valid timezone identifiers
const validTimezones = ['America/New_York', 'America/Los_Angeles', 'Europe/London', 'Europe/Paris', 'Asia/Tokyo', 'Asia/Shanghai', 'Australia/Sydney', 'Africa/Cairo', 'America/Chicago', 'America/Toronto', 'Asia/Dubai', 'Europe/Berlin', 'Pacific/Honolulu', 'Asia/Kolkata', 'Africa/Johannesburg', 'Asia/Jakarta', // Add more timezones as needed
];

// Arrays of sample first names and last names
const sampleFirstNames = ['John', 'Jane', 'Michael', 'Emily', 'David', 'Sarah'];
const sampleLastNames = ['Doe', 'Smith', 'Johnson', 'Brown', 'Williams', 'Jones'];

describe('User Controller', () => {
    describe('create', () => {
        // Create a test for each valid timezone
        validTimezones.forEach((timezone) => {
            it(`should create a new user in timezone ${timezone}`, async () => {
                // Randomly select a first name and last name from the sample arrays
                const randomFirstName = sampleFirstNames[Math.floor(Math.random() * sampleFirstNames.length)];
                const randomLastName = sampleLastNames[Math.floor(Math.random() * sampleLastNames.length)];

                // Arrange
                const req = {
                    body: {
                        email: randomFirstName + '' + randomLastName + '@gmail.com',
                        firstName: randomFirstName,
                        lastName: randomLastName,
                        birthday: new Date('1990-09-08T00:00:00.000Z'),
                        location: timezone,
                    },
                };
                const res = {
                    status: jest.fn().mockReturnThis(), json: jest.fn(),
                };

                // Act: Call the create function
                await userController.create(req, res);

                // Assert
                expect(res.status).toHaveBeenCalledWith(201); // Response status is 201 (Created)
                expect(res.json).toHaveBeenCalledWith({
                    success: true, message: 'User created successfully', user: {
                        email: randomFirstName + '' + randomLastName + '@gmail.com', firstName: randomFirstName, // Verify that the random first name is set correctly
                        lastName: randomLastName,   // Verify that the random last name is set correctly
                        birthday: new Date('1990-09-08T00:00:00.000Z'), location: timezone,
                    },
                });
            }, 10000); // Increased timeout to 10 seconds
        });
    });

    it('should handle errors during user creation', async () => {
        // Arrange
        const req = {
            body: {
                // Invalid data that will trigger an error during creation
                firstName: 'Invalid', // Example: Invalid data that may cause a database constraint violation
            },
        };
        const res = {
            status: jest.fn().mockReturnThis(), json: jest.fn(),
        };

        // Act: Call the create function
        await userController.create(req, res);

        // Assert
        expect(res.status).toHaveBeenCalledWith(500); // Response status is 500 (Internal Server Error)
        expect(res.json).toHaveBeenCalledWith(expect.objectContaining({
            success: false, message: 'Error creating user',
        }));
    });
});
