const userController = require("../controllers/userController");

describe('User Controller', () => {
    describe('delete', () => {
        it('should delete a user', async () => {

            const req = {
                body: {
                    userId: '64fa9cec4b18d86d06aa7a8f',
                },
            };

            const res = {
                status: jest.fn().mockReturnThis(), json: jest.fn(),
            };

            // Act: Call the create function
            await userController.delete(req, res);

            expect(res.status).toHaveBeenCalledWith(200); // Response status is 201 (Created)
            expect(res.json).toHaveBeenCalledWith({
                success: true, message: 'User deleted successfully',
            });
        });
    });
});
