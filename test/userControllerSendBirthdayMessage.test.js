const {PrismaClient} = require('@prisma/client');
const userController = require('../controllers/userController');
const {advanceTo, clear} = require('jest-date-mock');
const axios = require('axios');

// Mock PrismaClient
const prisma = new PrismaClient();

jest.mock('axios'); // Mock the entire axios module

describe('User Controller - sendBirthdayMessage', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    afterAll(() => {
        prisma.$disconnect();
        clear(); // Clear mocked date
    });

    it('should send birthday messages at 9:00 AM', async () => {
        // Mock the Axios POST request to always return a successful response
        axios.post.mockResolvedValue({status: 200});

        // Mock the user data from the database
        const mockUsers = [{
            id: '64faea305fade90ca9629893',
            email: 'DavidWilliams@gmail.com',
            firstName: 'David',
            lastName: 'Williams',
            birthday: '1990-09-08',
            location: 'Asia/Jakarta',
        }, // Add more mock user data as needed
        ];

        // Mock the behavior of Prisma's `user.findMany`
        prisma.user.findMany = jest.fn().mockResolvedValue(mockUsers);

        // Set the current date and time to 9:00 AM on a user's birthday
        advanceTo(new Date('1990-09-08T09:00:00'));

        const req = {};
        const res = {
            status: jest.fn().mockReturnThis(), json: jest.fn(),
        };

        await userController.sendBirthdayMessage(req, res);

        // Expect Axios to have been called with the correct payload
        expect(axios.post).toHaveBeenCalledWith('https://email-service.digitalenvision.com.au/send-email', {
            email: 'DavidWilliams@gmail.com', message: 'Hey, David, it\'s your birthday!',
        });

        // Expect the response to indicate success
        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).toHaveBeenCalledWith({message: 'Birthday messages sent successfully'});
    }, 20000);

    it('should handle API timeout and eventually succeed', async () => {
        // Mock axios.post to simulate a timeout on the first two calls
        let axiosCallCount = 0;
        axios.post.mockImplementation(() => {
            axiosCallCount++;
            if (axiosCallCount <= 2) {
                // Simulate a timeout on the first two calls
                return new Promise((resolve, reject) => {
                    setTimeout(() => {
                        reject(new Error('Timeout'));
                    }, 1);
                });
            } else {
                // Simulate a successful response on the third call
                return Promise.resolve({status: 200});
            }
        });

        const req = {};
        const res = {
            status: jest.fn().mockReturnThis(), json: jest.fn(),
        };

        await userController.sendBirthdayMessage(req, res);

        // Verify that the response indicates success
        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).toHaveBeenCalledWith({message: 'Birthday messages sent successfully'});
    }, 20000);


    it('should handle API random error and eventually succeed', async () => {
        // Mock axios.post to simulate a random error on the first two calls and succeed on the third call
        axios.post
            .mockRejectedValueOnce(new Error('Random Error 1'))
            .mockRejectedValueOnce(new Error('Random Error 2'))
            .mockResolvedValue({status: 200});

        const req = {};
        const res = {
            status: jest.fn().mockReturnThis(), json: jest.fn(),
        };

        await userController.sendBirthdayMessage(req, res);

        // Verify that the response indicates success
        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).toHaveBeenCalledWith({message: 'Birthday messages sent successfully'});
    }, 20000);

    it('should prevent handling race conditions and duplicate messages', async () => {
        // Mock the Axios POST request to always return a successful response
        axios.post.mockResolvedValue({status: 200});

        // Mock the user data from the database
        const mockUsers = [{
            id: '64faea305fade90ca9629893',
            email: 'DavidWilliams@gmail.com',
            firstName: 'David',
            lastName: 'Williams',
            birthday: '1990-09-08',
            location: 'Asia/Jakarta',
        }, {
            id: '64faea305fade90ca9629894',
            email: 'JohnDoe@gmail.com',
            firstName: 'John',
            lastName: 'Doe',
            birthday: '1990-09-08',
            location: 'Asia/Jakarta',
        },];

        // Mock the behavior of Prisma's `user.findMany`
        prisma.user.findMany = jest.fn().mockResolvedValue(mockUsers);

        // Set the current date and time to 9:00 AM on a user's birthday
        advanceTo(new Date('1990-09-08T09:00:00'));

        const req = {};
        const res = {
            status: jest.fn().mockReturnThis(), json: jest.fn(),
        };

        // Simulate sending messages from multiple users at the same time
        const sendMessages = async () => {
            await Promise.all([userController.sendBirthdayMessage(req, res), userController.sendBirthdayMessage(req, res),]);
        };

        await sendMessages();

        // Expect Axios to have been called twice with the correct payload
        expect(axios.post).toHaveBeenCalledTimes(2);
        expect(axios.post).toHaveBeenCalledWith('https://email-service.digitalenvision.com.au/send-email', {
            email: 'DavidWilliams@gmail.com', message: 'Hey, David, it\'s your birthday!',
        });

        // Expect the response to indicate success
        expect(res.status).toHaveBeenCalledWith(200);
        expect(res.json).toHaveBeenCalledWith({message: 'Birthday messages sent successfully'});
    }, 10000);
});
