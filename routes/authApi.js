const express = require("express");

const router = express.Router();

const { catchErrors } = require("../handlers/errorHandlers");
const {
    isValidToken,
    register,
    login,
    logout
} = require("../controllers/authController");

router.route("/login").post(catchErrors(login));
router.route("/isValidToken").post(catchErrors(isValidToken));
router.route("/register").post(catchErrors(register));
router.route("/logout").post(catchErrors(logout));

module.exports = router;
