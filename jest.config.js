module.exports = {
    // The test environment
    testEnvironment: 'node',

    // The root directory of your project
    // Change this to match your project's directory structure
    roots: ['<rootDir>'],

    // Test timeout (adjust as needed)
    testTimeout: 10000, // Set a global test timeout of 10 seconds

    // Test file patterns
    testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.(jsx?|tsx?)$',

    // Module file extensions for importing modules
    moduleFileExtensions: ['js', 'json', 'jsx', 'ts', 'tsx', 'node'],

    // Other Jest configuration options...

    // You can add more configuration options as needed.
};
